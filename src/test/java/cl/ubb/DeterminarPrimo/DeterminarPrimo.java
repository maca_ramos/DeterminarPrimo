package cl.ubb.DeterminarPrimo;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Test;

public class DeterminarPrimo {

	@Test
	public void IngresarCeroRetornarPrimoFalso() {
		/*arrange*/
		NumeroPrimo primo = new NumeroPrimo();
		boolean Resultado;
		/*act*/
		Resultado=primo.DeterminarPrimo(0);
		/* assert */
		assertThat(Resultado,is(false));
	}
	
	@Test
	public void IngresarUnoRetornarPrimoFalso() {
		/*arrange*/
		NumeroPrimo primo = new NumeroPrimo();
		boolean Resultado;
		/*act*/
		Resultado=primo.DeterminarPrimo(1);
		/* assert */
		assertThat(Resultado,is(false));
	}
	
	@Test
	public void IngresarDosRetornarPrimoVerdadero() {
		/*arrange*/
		NumeroPrimo primo = new NumeroPrimo();
		boolean Resultado;
		/*act*/
		Resultado=primo.DeterminarPrimo(2);
		/* assert */
		assertThat(Resultado,is(true));
	}
	
	@Test
	public void IngresarTresRetornarPrimoVerdadero() {
		/*arrange*/
		NumeroPrimo primo = new NumeroPrimo();
		boolean Resultado;
		/*act*/
		Resultado=primo.DeterminarPrimo(3);
		/* assert */
		assertThat(Resultado,is(true));
	}

	@Test
	public void IngresarCuatroRetornarPrimoFalso() {
		/*arrange*/
		NumeroPrimo primo = new NumeroPrimo();
		boolean Resultado;
		/*act*/
		Resultado=primo.DeterminarPrimo(4);
		/* assert */
		assertThat(Resultado,is(false));
	}
	
	@Test
	public void IngresarCincoRetornarPrimoVerdadero() {
		/*arrange*/
		NumeroPrimo primo = new NumeroPrimo();
		boolean Resultado;
		/*act*/
		Resultado=primo.DeterminarPrimo(5);
		/* assert */
		assertThat(Resultado,is(true));
	}
	
	@Test
	public void IngresarSeisRetornarPrimoFalso() {
		/*arrange*/
		NumeroPrimo primo = new NumeroPrimo();
		boolean Resultado;
		/*act*/
		Resultado=primo.DeterminarPrimo(6);
		/* assert */
		assertThat(Resultado,is(false));
	}
	
	@Test
	public void IngresarSieteRetornarPrimoVerdadero() {
		/*arrange*/
		NumeroPrimo primo = new NumeroPrimo();
		boolean Resultado;
		/*act*/
		Resultado=primo.DeterminarPrimo(7);
		/* assert */
		assertThat(Resultado,is(true));
	}

}
